$.material.init();

var json = {
    title: "Individual Activity: Are You Productive?",
    showProgressBar: "top",
    pages: [{
        questions: [{
            type: "matrix",
            name: "header",
            title: "Individual Activity: Are You Productive?",
            columns: [{
                value: 1,
                text: "1"
            }, {
                value: 2,
                text: "2"
            }, {
                value: 3,
                text: "3"
            }],
            rows: [{
                value: "spend",
                text: "I spend 10–15 minutes planning my day."
            }, {
                value: "check",
                text: "I check every email as soon as it arrives."
            },{
                value: "day",
                text: "My day is organized by the key results I need to achieve."
            }, {
                value: "desk",
                text: "My desk and files are a mess."
            }, {
                value: "leave",
                text: "I leave open pockets of time in my schedule."
            }, {
                value: "late",
                text: "I’m habitually late."
           }, {
                value: "focus",
                text: "I focus on one activity at a time."
            }, {
                value: "done",
                text: "I multi-task to try to get more done."
            }, {
                value: "take",
                text: "I take mini-breaks throughout the day."
            }, {
                value: "big",
                text: "I usually procrastinate on big projects."
            }, {
                value: "projects",
                text: "I actively seek out projects that stretch my skills."
            }, {
                value: "rut",
                text: "I feel I am in a rut at work."
            }]
        }]
        }]
   }     
Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrapmaterial";

var survey = new Survey.Model(json);

survey.onComplete.add(function(result) {

	var sum = 0;
	var rows = Object.values(result.data);
	for(var i = 0; i < rows.length; i++){
		var row = Object.values( rows[i] );
		for(var j = 0; j < row.length; j++) {
			var val = row[j];
			sum += parseInt(val);
		}
	}


function myFunction(sum) 

{
    var scoring;
    if (sum <= 40 && sum >= 36) {
        scoring = "You handle change very well! You can use what you learn in this course to become even better.";
    } else if (sum <= 35 && sum >= 31) {
        scoring = "You handle change well, but you can still improve.";
    } else if (sum <= 30 && sum >= 26) {
        scoring = "You are on your way to being able to handle change successfully, but there are some areas you need to work on. ";
    } else if (sum <= 25 && sum >=21) {
        scoring = "Your ability to handle change is not bad, but you could do better.";
    } else if (sum <= 20 && sum >=16) {
        scoring = "Your ability to handle change could use improvement.";
    } else if (sum <= 15 && sum >=0) {
        scoring = "You may have difficulty handling change and uncertainty.";
    } else {
        scoring = "Error";
    }
    
document.querySelector('#result').innerHTML = "Your Score " + sum + "<br>" + "According to the survey, " + scoring;

} 

myFunction(sum);

});
       
survey.render("surveyElement");